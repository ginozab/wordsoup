.data

	space1:		.space		10
	newline:	.asciiz		"\n"
	period:		.asciiz		"."
	
.text
	li $t0, 0
	li $t1, 'a'
	la $s1, space1
	LOOPADD:
		beq $t0, 10, print
		sb $t1, 0($s1)
		addi $s1, $s1, 1
		addi $t0, $t0, 1
		
		j LOOPADD
		
	print:
	la $s1, space1
	
	li $v0 4
	move $a0, $s1
	syscall
	jal RESTORE
	
	j print1
	
	RESTORE:
	#PROLOGUE
	subi $sp, $sp, 4
	sw $ra, 0($sp)
	
	#MAIN BODY
	li $t3, 0
	
	LOOPRESTORE:
		beq $t3, 10, end_restore
		sb $zero, 0($s1)
		addi $s1, $s1, 1
		addi $t3, $t3, 1
		
		j LOOPRESTORE
	
	end_restore:
		
	#EPILOGUE
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	#RETURN
	jr $ra
		
	print1:
	
	la $s1, space1
	
	
	
	li $v0, 4
	la $a0, period
	syscall
	
	li $v0, 4
	move $a0, $s1
	syscall
	
	li $v0, 4
	la $a0, period
	syscall
	
	li $v0, 4
	la $a0, newline
	syscall
	
	LOOPADD1:
		beq $t0, 10, print2
		sb $t1, 0($s1)
		addi $s1, $s1, 1
		addi $t0, $t0, 1
		
		j LOOPADD1
		
	print2:
	la $s1, space1
	
	li $v0 4
	move $a0, $s1
	syscall
	
	li $v0, 10
	syscall
	