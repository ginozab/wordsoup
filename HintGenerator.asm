.data

	WELCOME:	.asciiz		"Welcome to WordSoup!\n"
	FIRST_PROMPT:	.asciiz		"\nI am thinking of a word. The word is "
	FIRST_PROMPT1:	.asciiz		". Round score is "
	FIRST_PROMPT2:	.asciiz		"Guess a letter?\n"
	WRONG_GUESS:	.asciiz		"\nNo! The word is "
	LOST_ROUND:	.asciiz		"You lost the round!" 
	FINAL_STATE:	.asciiz		" The final state of you word was:\n"
	CORRECT_WORD:	.asciiz		"\nCorrect word was:\n"
	YOUR_ROUNDSCORE:.asciiz		"\nYour round score is "
	GAME_TALLY:	.asciiz		". Game tally is "
	PLAY_AGAIN:	.asciiz		"\nDo you want to play again? (y/n) "
	CORRECT:	.asciiz		"\nCorrect! The word is "
	CORRECT_ROUND:	.asciiz		"Correct!"
	FORFEIT:	.asciiz		"You forfeited the round!\n"
	FINAL_STATE1:	.asciiz		"The final state of you word was:\n"
	HINT:		.asciiz		"Here's a hint! You are now out of hints.\n"
	THE_WORD:	.asciiz		"The word is "
	OUT_OF_HINTS:	.asciiz		"I'm sorry, you are out of hints!\n"
	WHAT_GUESS:	.asciiz		"What is your guess?\n"
	CORRECT_GUESS:	.asciiz		"You guessed the word correctly!\n"
	CORRECT_GUESS1:	.asciiz		", doubled to "
	TERRIBLE_GUESS:	.asciiz		"That guess was terrible!\n"
	TALLY_PENALTY:	.asciiz		"Your Game Tally is being penalized by "
	word:		.asciiz		"hello"
	ast:		.asciiz		"_*___"
	space:		.asciiz		" "
.text

	la $s6, 0		#need to store in $s register in game loop
	li $a0, 5		#need to store in $s register in game loop
	la $s1, word
	la $s2, ast
	jal HINT_GENERATOR
	move $a1, $v0
	
	move $a1, $s2
	jal PRINTSPACE
	
	li $v0, 10
	syscall
	
	HINT_GENERATOR:
	#PROLOGUE
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	#MAIN BODY
		HINT_LOOP:
		beq $s6, 1, OUT_OF_HINT	#check if uer is out of hints
		move $s0, $a0			
		jal RANDOMNUM			#generate random number between 0 and size
		move $t0, $v0			#move random number into $t0
		
		# add $t0 to both pointers
		add $s1, $s1, $t0	
		add $s2, $s2, $t0
		#load both bytes of strings
		lb $t1, 0($s1)
		lb $t2, 0($s2)
		#if the pointer is a * try again
		beq $t2, '*', HINT_CONT
		sb $t1, 0($s2)			# if not a * insert character as hint
		j HINT_PRINT
		HINT_CONT:
		#put pointers back to zero if *
		sub $s1, $s1, $t0
		sub $s2, $s2, $t0
		j HINT_LOOP
		
		OUT_OF_HINT:
		li $v0, 4
		la $a0, OUT_OF_HINTS
		syscall
		
		li $v0, 4
		la $a0, THE_WORD
		syscall
		j HINT_END
		
	HINT_PRINT:
	li $v0, 4
	la $a0, HINT
	syscall
	
	li $v0, 4
	la $a0, THE_WORD
	syscall
	#decrement round Score here
	#
	#
	#add to the Hint counter here
	#
	#
	
	HINT_END:
	#EPILOGUE
	lw $s2, 8($sp)
	lw $s1, 4($sp)
	lw $ra, 0($sp)
	addi $sp, $sp, 12
	
	move $v0, $s2
	#RETURN
	jr $ra
	
	
	RANDOMNUM:
		#PROLOGUE
		subi $sp, $sp, 12
		sw $ra, 0($sp)
		sw $a0, 4($sp)
		sw $a1, 8($sp)
		
		#MAIN BODY
		li $v0, 30
		syscall
		#a0 has sort of ranom number based on system time
	
		#a1 maximum value
		move $a1, $s0
	
		#random number generation
		li $v0, 42
		syscall
		#a0 has your random number
		
		#return random number
		move $v0, $a0
		
		#EPILOGUE
		lw $a1, 8($sp)
		lw $a0, 4($sp)
		lw $ra, 0($sp)
		addi $sp, $sp, 12
		#RETURN
		jr $ra

	PRINTSPACE:
		#prologue
		subi $sp, $sp, 16
		sw $ra, 0($sp)
		sw $a0, 4($sp)
		sw $a1, 8($sp)
		sw $a2, 12($sp)
		
		#main body
			la $a2, space
			li $t1, 0
			COUNT:				#counts number of characters
				lbu $t3, 0($a1)
				beqz $t3, prints
				addi $a1, $a1, 1
				addi $t1, $t1, 1		#number of characters in $t1
				j COUNT
				
				prints:
				lw $a1, 8($sp)			#sets $a1 back to original
				li $t2, 0			# start counter
			PLOOP:
				beq $t2, $t1, end		#counter check
				lb $t4, 0($a1)			#load character in $a1
				
				#print out charcater
				move $a0, $t4			
				li $v0, 11
				syscall
				
				#print space
				move $a0, $a2
				li $v0, 4
				syscall
				
				#add to counters
				addi $a1, $a1, 1
				addi $t2, $t2, 1
				
				j PLOOP
				
		end:
		#epilogue
		lw $a2, 12($sp)
		lw $a1, 8($sp)
		lw $a0, 4($sp)
		lw $ra, 0($sp)
		addi $sp, $sp, 16
		#return
		jr $ra
	
	
	
	
	
	